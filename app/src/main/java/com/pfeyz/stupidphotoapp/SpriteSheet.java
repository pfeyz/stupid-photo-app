package com.pfeyz.stupidphotoapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

// TODO: next action items: implement crop, resize, repack and pasteImage

public class SpriteSheet {
    // the number of rows in the grid (number of rows always == number of cols)
    private int gridRows;
    // the number of cells that actually contain image data so far
    private int cellsUsed;

    private int imageWidth;
    private int imageHeight;

    // the actual spritesheet
    private Bitmap spriteSheet;

    public SpriteSheet(){
        // load columns and numUsed from
        spriteSheet = BitmapFactory.decodeFile(Utils.getOutputImageUri().toString());
        imageWidth = imageHeight = 960;

        // TODO: read cellsUsed and gridRows from stored datafile
    }

    private int numCells() {
        return gridRows * gridRows;
    }

    private void repackSprite() {
        gridRows++;
        Bitmap[] images = new Bitmap[numCells()];
        // we can probably use Bitmap.getPixels to extract the individual pre-existing images
        // https://developer.android.com/reference/android/graphics/Bitmap.html#getPixels(int[],%20int,%20int,%20int,%20int,%20int,%20int)
        // getPixels(int[] pixels, int offset, int stride, int x, int y, int width, int height)

        // wait no I think createBitmap might be better

    }

    private void cropImage(Bitmap newImage) {
    }

    private void resizeImage(Bitmap newImage) {
    }

    private void pasteImage(Bitmap newImage, int targetX, int targetY) {
        // use setPixels
    }

    public void addImage(Bitmap newImage){
        if(cellsUsed == numCells()){
            repackSprite();
            addImage(newImage);
        } else {
            int targetRow = (int) Math.floor(numCells() / gridRows);
            int targetCol =  numCells() % gridRows;
            int targetX = targetRow * (imageWidth / gridRows);
            int targetY = targetCol * (imageHeight / gridRows);
            cropImage(newImage);
            resizeImage(newImage);
            pasteImage(newImage, targetX, targetY);
        }
        cellsUsed++;
    }
}

