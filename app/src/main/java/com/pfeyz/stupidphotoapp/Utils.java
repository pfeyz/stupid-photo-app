package com.pfeyz.stupidphotoapp;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Created by root on 2/26/17.
 */

public class Utils {

    /**
     * Create a file Uri for saving an image or video
     */
    public static Uri getOutputImageUri() {
        Uri thing = Uri.fromFile(getOutputImageFile());
        return thing;
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputImageFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        return new File(mediaStorageDir.getPath() + File.separator + "IMG" + ".jpg");

    }
}
